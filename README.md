# AI4CE Repo Template

## Setup
To start out you only need to suffice the following requirements:
1. Have Python 3.11 installed and set as your default Python
2. Having Poetry installed, which can be achieved over pip with the command 'pip install poetry'


Initial setup with poetry
For the initial setup, it is sufficient to simply run the two commands below.
Poetry shell will start an instance of a virtual environment of poetry.
Poetry install will install and/or modify the existing environment, taking care of dependencies for you.
```
poetry install
poetry shell
```

## Development Routine
### Run your code
To run your code, you need to execute it in the virtual environment of poetry. There are at least 2 ways to achieve this.
The most straightforward way is to run it on your console as normal, simply attaching 'poetry run' as a prefix.
Example:
```
poetry run python your_project_name/main.py
```
The other option is to set the installed poetry venv in your IDE. How this is done depends on your IDE, but you would usually have to find the path to the location where the environment is installed to and set it as a location for a Python runtime environment in your IDE

### Test your code
To test your code you can use pytest, which comes installed by the poetry environment. You should write tests extensively. An example test can be found in the dedicated tests folder and can be run like this:
```
poetry run pytest
```
For more arguments on how to run tests you can refer to the pytest documentation.

For more comprehensive testing, Hypothesis has been added to the testing environment. Hypothesis allows for so-called property based testing by comfortably letting you test every conceivable input and discover edge cases for inputs. The use of Hypothesis is therefore highly encouraged to ensure a better code coverage of your tests!
An example of how a Hypothesis test can look like is found in the tests class, but you should refer to the official documentation for further details: https://hypothesis.readthedocs.io/en/latest/index.html
Hypothesis runs piggy-back on any existing testing-tool and thus will be run together with pytest when invoking the command above!

### Check your changes for style- and convention-compliance
To ensure a common style in code writing for readability and thus reusability, we employ pre-commit, which is a tool that can host various sub-tools (so called hooks) to check not only your code but also documentation for adherence to coding conventions and can to some degree automatically fix errors. You can run pre-commit hooks like this:
```
pre-commit run -a
```

### Commit and push your changes
If everything checks out in the tests and pre-commit, then your code may be ready for pushing. This is done by the three steps:
1. Add
2. Commit
3. Push
```
git add <changes>
git commit -m "<message>"
git push
```
The last step may need resolving, like merging files.


### Generate code documentation
This template also provides you with a powerful tool to generate code documentation: Sphinx
Sphinx can convert your entire project into an easily comprehensible documentation of that project in a few selected formats.
The most prominent one is as html files, but pdf are also an option. This tutorial will focus on how to generate pdf. For more info about sphinx, please check out the official documentation: https://www.sphinx-doc.org

Most things are already setup to automatically generate a documentation for your entire project, but a few changes should be made before creating your own:
In the docs folder the file conf.py contains the configuration. Here you should change the author and project name and can freely adjust the version number. This is also the place to add extensions or change the theme of the html, but we recommend keeping the theme as to retain a project over-arching uniformity.
After that you only need to execute two commands to create the documentation for each module that has an __init__.py file (thus make sure whether you have own for every module to be documented)!
```
sphinx-apidoc -o docs .
cd docs
make html
cd ..
```
It may also be necessary to install the functionality to execute the make command, which is operating-system specific. If everything went well, then you should be able to find the corresponding html files inside the _build folder. Now you simply have to open them in a browser of your choice!

### Docker: Deploy your code
Ultimately the goal of most, if not all of the repositories, is for them to deployed as cog-wheels of an overarching service. Docker provides the functionality to host each of these sub-projects as so-called containers with excellent support to host them on a server, utilizing a container-system in which various applications can be run in different containers as services, able to interact with each other.

#### Docker fundamentals

To fascilitate docker in each of the projects, it has been added to this template. In order to use docker yourself, you need to prepare a few things:
1. Install docker on your device: This can be done by console (Linux), `sudo apt install -y docker docker-compose`, or with docker desktop (Windows) and can be downloaded here: https://www.docker.com/. Your IDE may also provide docker support via extensions for ease of use. It is important, that you also enable WSL2 in the settings, if you are using WSL. Lastly you need to add yourself as a user to the docker user-group, are run docker commands as and admin. Otherwise the docker command will fail due to denied permissions. Also make sure, that docker is always running, when you attempt to build docker containers.
2. Modify the dockerfile template: The dockerfile dirigates the way docker-containers are compiled and run. It already contains a set of instructions that, when run can execute the code of the template. The necessary modifications may highly depend on your project. Changes that are definitely necessary are: Change the executable location from your_project_name to your actual project name. You may also be able to change the python version from 3.11 to 3.11-alpine or another version for an even slimmer container-image, if your library requirements permit them. You will find out about the compatibility of the python version, when you try to build your docker file, leading to the next step. For more details about each of the docker commands, I recommend to refer to the official docker documentation, but here are a few fundamental commands:
`FROM` base image
`RUN` command
`COPY` folder/file-on-host folder/file-in-container (source destination)
`WORKDIR` translates to the `cd` aka change directory command
`ENTRYPOINT` program to be run
`CMD` list of arguments passed to the ENTRYPOINT program

Additional commands are
`ENV` specify environment variables (will be overwritten by variables specified during deployment)
`USER` to run the program as (you'll have to create the user first)

Things to note about the Dockerfile:
It's a two staged build, as you can see by the two `FROM` commands. Since we don't need poetry to actually run our program, we don't want it installed in our final container.
    - So our first stage will install poetry, copy the pyproject.toml into the container so that poetry can create the requirements.txt
    - We then start a new container image with a new base image (also python-alpine), import the requirements.txt generated by the first stage, install only the requirements for running our program and do the rest
Creating a non-root user. This is just best practice

3. Build your docker container: In this step, you simply need to build the container according to your docker file. The easiest way to do this is right-click on the docker file and select "Build image", provided that you have a docker extension installed. Alternatively you can run this over your console:
```
docker image build --pull --file '/*project location on your device*/*your project name*/Dockerfile' '/*project location on your device*/*your project name*'
```
If this command runs successfully, then you have built a docker container, which you may run. The folder .vscode contains the build configurations for VSCode users.
4. Run your docker container: Now finally you just have to run your docker container. This is a trivial affair over docker desktop: Simply go to "Images", find your container in the list, and press the "play"-button in the actions column. Docker will ask you about running-configurations, but you can likely leave everything blank. There is also an option to do this over console:
```
docker run myprojectname:versiontag
```

#### Streamlit GUI
This template also contains GUI capabilities enabled by streamlit. Streamlit is a framework made for providing a toolbox of GUI elements that you can use to wrap your projects functionalities into a user friendly interface. For a tutorial on how to use streamlit in detail and all its functions, please refer to their official docs page: https://docs.streamlit.io/
For a simple example, take a look at gui.py in your_project_name/gui.
Streamlit can directly be run by
```
poetry run streamlit run your_project_name/gui/gui.py
```
However when building docker containers, the container by default utilizes streamlit. You can build and run these containers with the following commands:
``` bash
docker build -t your_project_name:latest .
docker run -p 8599:8599 your_project_name:latest
```
If you decide to use docker desktop, make sure to put 8599 as the port, unless you want to define a different port, which you also need to set in the dockerfile.
After launching the container, go to http://localhost:8599 to view your running application!
