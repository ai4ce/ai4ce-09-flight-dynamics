import streamlit as st

st.set_page_config(
    page_title="Hello",
    page_icon="👋",
)

st.write("# Welcome to the Mission Analysis Web App! This tool is designed to help you analyze and visualize orbital missions with ease! 👋")

st.sidebar.success("Select a demo above.")

st.markdown(
    """
    ## Features:

1. **Orbit Visualization:** Explore and visualize the trajectories of your missions in an interactive way.

2. **Launch Window Calculator:** Determine optimal launch windows for your missions based on celestial mechanics principles.

3. **Lambert Problem Solver:** Solve the Lambert problem to find fuel-optimal transfer orbits between celestial bodies.

## How to Use:

1. **Launch Window Calculator:**
   - Select your target celestial body (e.g., satellites).
   - Input relevant parameters such as position, velocity, or orbital elements.
   - Get real-time information about upcoming launch windows and optimal transfer trajectories.

2. **Orbit Visualization:**
   - Visualize your mission trajectory in space.
   - Explore different views and perspectives to better understand your mission's orbital dynamics.

3. **Lambert Problem Solver:**
   - Solve Lambert's problem to calculate fuel-optimal trajectories between two positions in space.
   - Obtain insights into the timing and energy requirements for your interplanetary transfers.

## Getting Started:

1. Input your mission parameters in the provided forms.
2. Explore the visualizations and launch window predictions.
3. Use the Lambert problem solver to optimize your mission's trajectory.

"""
)