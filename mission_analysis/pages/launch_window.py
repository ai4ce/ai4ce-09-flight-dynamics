from poliastro.bodies import Earth, Sun
from poliastro.twobody import Orbit
from astropy import units as u
import math
import streamlit as st
import matplotlib.pyplot as plt
from poliastro.plotting.orbit import OrbitPlotter

# Get user input for orbital parameters
a = st.number_input("Enter semi-major axis (AU)", value=  -6778.137  )
ecc = st.number_input("Enter eccentricity (one)", value=0)
inc = st.number_input("Enter inclination (deg)", value=50)
raan = st.number_input("Enter longitude of the ascending node (deg)", value=30)
argp = st.number_input("Enter argument of perigee (deg)", value=0)
nu = st.number_input("Enter true anomaly (deg)", value=0)

latitude = st.number_input("Enter the lattitude (deg)", value=28.5) 
longitude = st.number_input("Enter the longitude (deg)", value=-80) 

la = st.number_input("Enter the minimum range of launch azimuth (deg)", value=35)
lb = st.number_input("Enter the maximum range of launch azimuth (deg)", value=120)

a = a << u.km
ecc = ecc << u.one
inc = inc << u.deg
raan = raan << u.deg
argp = argp << u.deg
nu = nu << u.deg
latitude = latitude << u.deg
longitude = longitude << u.deg

# Define the inclination auxiliary angle
alpha = inc
# Convert semi-major axis to AU
a = abs(a.to(u.AU))
# Defining the orbit
orbit = Orbit.from_classical(
    Earth,
    a,
    ecc * u.one,
    inc,
    raan,
    0 * u.deg,  # Argument of Perigee (undefined in this case)
    0 * u.deg,  # True Anomaly (0 for a known state)
)

# Function to calculate gamma
def calculate_gamma(latitude, alpha):
    # Convert angles to radians
    alpha_rad = math.radians(alpha)
    latitude_rad = math.radians(latitude)

    # Calculate gamma
    cos_alpha = math.cos(alpha_rad)
    cos_latitude = math.cos(latitude_rad)

    sin_gamma = cos_alpha / cos_latitude
    gamma = math.asin(sin_gamma)

    # Convert gamma to degrees
    gamma_deg = math.degrees(gamma)
    return gamma_deg

# Function to calculate delta
def calculate_delta(gamma, alpha):
    gamma_rad = math.radians(gamma)
    alpha_rad = math.radians(alpha)

    cos_gamma = math.cos(gamma_rad)
    sin_alpha = math.sin(alpha_rad)

    cos_delta = cos_gamma / sin_alpha
    delta = math.acos(cos_delta)

    delta_deg = math.degrees(delta)
    return delta_deg


# Calculate gamma based on latitude and alpha
gamma_angle = calculate_gamma(latitude.value, alpha.value)
delta_angle = calculate_delta(gamma_angle, alpha.value)
print(f"Delta: {delta_angle} degrees")

#launch azimuth for ascending & descending node
def beta_ascending(gamma_angle):
    return gamma_angle
beta_an = beta_ascending(gamma_angle)
st.write(f"Launch azimuth for ascending node: {round(beta_an,2)} degrees")

def beta_descending(gamma_angle):
    beta_dn = 180-gamma_angle
    return beta_dn
beta_dn = beta_descending(gamma_angle)
st.write(f"Launch azimuth for descending node: {round(beta_dn,2)} degrees")

def calculate_lwst_ascending(raan, delta):
    lwst_an = raan + delta*u.deg
    lwst_an_hours = lwst_an*(u.h/(15*u.deg))
    return lwst_an_hours
lwst_an_hours = calculate_lwst_ascending(raan, delta_angle)
print(lwst_an_hours)

def calculate_lwst_descending(raan, delta):
    lwst_dn = raan + 180*u.deg - delta*u.deg
    lwst_dn_hours = lwst_dn * (u.h / (15*u.deg))  # Conversion to hours
    return lwst_dn_hours
lwst_dn_hours = calculate_lwst_descending(raan, delta_angle)
print(lwst_dn_hours)

lst = 12.00 * u.h
def calculate_wait_times(lst, lwst_an_hours, lwst_dn_hours):
    wait_time_an = lwst_an_hours - lst
    if wait_time_an < 0:
        wait_time_an += 24 * u.h
    
    wait_time_dn = lwst_dn_hours - lst
    if wait_time_dn < 0:
        wait_time_dn += 24 * u.h
    
    return wait_time_an, wait_time_dn
wait_time_an, wait_time_dn = calculate_wait_times(lst, lwst_an_hours, lwst_dn_hours)

st.write(f"Wait time of ascending node opportunity: {round(wait_time_an.value, 2)} hours")
st.write(f"Wait time of ascending node opportunity: {round(wait_time_dn.value, 2)} hours")


def launch_window(beta_an, beta_dn, wait_time_an, wait_time_dn):
    chosen_wait_time = None
    if la  <= beta_an <= lb :
        chosen_wait_time = wait_time_an
    elif la  <= beta_dn <= lb :
        chosen_wait_time = wait_time_dn    
    return chosen_wait_time

# Call the function using the calculated values
result = launch_window(beta_an, beta_dn, wait_time_an, wait_time_dn)
result = result.to(u.h).value
# Print the chosen wait time based on the launch azimuth conditions
if result is not None:
    st.write(f"Chosen wait time: {round(result, 2)} is the wait time of ascending node which falls within the specified range of launch azimuth {la} and {lb}")
else:
    st.write("No suitable launch azimuth found within the specified range.")


# Plot the orbit using poliastro's StaticOrbitPlotter
op = OrbitPlotter()
op.plot(orbit)
st.pyplot(plt)
