from astropy import units as u
from poliastro.bodies import Earth, Mars, Sun
from poliastro.twobody import Orbit
from poliastro.plotting.orbit import OrbitPlotter
import matplotlib.pyplot as plt
import streamlit as st


# Data for Mars at J2000 from JPL HORIZONS

# Get user input for orbital parameters
a = st.number_input("Enter semi-major axis (AU)", value=1.523679)
ecc = st.number_input("Enter eccentricity (one)", value=-0.093315)
inc = st.number_input("Enter inclination (deg)", value=1.85)
raan = st.number_input("Enter longitude of the ascending node (deg)", value=49.562)
argp = st.number_input("Enter argument of perigee (deg)", value=286.537)
nu = st.number_input("Enter true anomaly (deg)", value=23.33)

a = a << u.AU
ecc = ecc << u.one
inc = inc << u.deg
raan = raan << u.deg
argp = argp << u.deg
nu = nu << u.deg

#An Orbit object (orbit) is created using the Orbit.from_classical method from poliastro
orbit = Orbit.from_classical(Sun, a, ecc, inc, raan, argp, nu)
#calculates orbital period in days
orbit.period.to(u.day)
#<Quantity 686.9713888628166 d>
orbit.v
#<Quantity [  1.16420211, 26.29603612,  0.52229379] km / s>

# Display the orbit details
st.write(f"Orbit details: {orbit}")
st.write(f"Epoch: {orbit.epoch.iso}")
st.write(f"Orbit velocity: {orbit.v}")
st.write(f"Orbit position: {orbit.r}")
st.write(f"Orbit period: {orbit.period.to(u.day)}")

op = OrbitPlotter()
op.plot(orbit)
st.pyplot(plt)
