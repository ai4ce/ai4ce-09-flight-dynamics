from astropy import units as u
from astropy import time
import streamlit as st
import numpy as np
import matplotlib.pyplot as plt
from poliastro import iod
from poliastro.bodies import Earth, Mars, Sun
from poliastro.ephem import Ephem
from poliastro.maneuver import Maneuver
from poliastro.twobody import Orbit
from poliastro.util import time_range
from poliastro.plotting.orbit import OrbitPlotter

# The date of the launch was on November 26, 2011 at 15:02 UTC and landing was on August 6, 2012 at 05:17 UTC. 
# We compute then the time of flight, which is exactly what it sounds:
date_launch = time.Time("2011-11-26 15:02", scale="utc").tdb
date_arrival = time.Time("2012-08-06 05:17", scale="utc").tdb

earth = Ephem.from_body(Earth, time_range(date_launch, end=date_arrival))
mars = Ephem.from_body(Mars, time_range(date_launch, end=date_arrival))

# Solve for departure and target orbits
ss_earth = Orbit.from_ephem(Sun, earth, date_launch)
#print(ss_earth) 1 x 1 AU x 23.4 deg (HCRS) orbit around Sun (☉) at epoch 2011-11-26 15:03:06.183 (TDB)
ss_mars = Orbit.from_ephem(Sun, mars, date_arrival)
#print(ss_mars) 1 x 2 AU x 24.7 deg (HCRS) orbit around Sun (☉) at epoch 2012-08-06 05:18:07.183 (TDB)

# Solve for the transfer maneuver with function lambert
man_lambert = Maneuver.lambert(ss_earth, ss_mars)
dv_a, dv_b = man_lambert.impulses
#print(man_lambert) Number of impulses: 2, Total cost: 6.889865 km / s

# Get the transfer and final orbits
ss_trans, ss_target = ss_earth.apply_maneuver(man_lambert, intermediate=True)
#print(ss_trans,ss_target)
#1 x 2 AU x 22.9 deg (HCRS) orbit around Sun (☉) at epoch 2011-11-26 15:03:06.183 (TDB)
#1 x 2 AU x 24.7 deg (HCRS) orbit around Sun (☉) at epoch 2012-08-06 05:18:07.183 (TDB)

op = OrbitPlotter()
op.plot(ss_earth, label="Earth", color="navy")
op.plot(ss_mars, label="Mars", color="darkred")
op.plot(ss_trans, label="Long transfer", color="green")
#plt.savefig("orbit_plot.png")
st.pyplot(plt.gcf()) 