from astropy import units as u
from poliastro.bodies import Earth, Mars, Sun
from poliastro.twobody import Orbit
from poliastro.plotting.orbit import OrbitPlotter
import matplotlib.pyplot as plt
import streamlit as st

# Data from Curtis, example 4.3
#r = [-6045, -3490, 2500] << u.km
#v = [-3.457, 6.618, 2.533] << u.km / u.s

# Get user input for position vector (r)
rx = st.number_input("Enter x-component of position (km)", value=-6045)
ry = st.number_input("Enter y-component of position (km)", value=-3490)
rz = st.number_input("Enter z-component of position (km)", value=2500)

# Get user input for velocity vector (v)
vx = st.number_input("Enter x-component of velocity (km/s)", value=-3.457)
vy = st.number_input("Enter y-component of velocity (km/s)", value=6.618)
vz = st.number_input("Enter z-component of velocity (km/s)", value=2.533)

r = [rx, ry, rz] << u.km
v = [vx, vy, vz] << u.km / u.s

#The input vectors are converted into an Orbit object using the Orbit.from_vectors method from poliastro.
orbit = Orbit.from_vectors(Earth, r, v)
#7283 x 10293 km x 153.2 deg (GCRS) orbit around Earth (♁) at epoch J2000.000 (TT)
orbit.epoch
#<Time object: scale='tt' format='jyear_str' value=J2000.000>
orbit.epoch.iso
#'2000-01-01 12:00:00.000'
orbit.get_frame()
#<GCRS Frame (obstime=J2000.000, obsgeoloc=(0., 0., 0.) m, obsgeovel=(0., 0., 0.) m / s)>

# Display the orbit details
st.write(f"Orbit details: {orbit}")
st.write(f"Epoch: {orbit.epoch.iso}")

op = OrbitPlotter()
op.plot(orbit)
st.pyplot(plt)