from sys import path
import pages.tle_data.planetary_data as pd
import pages.tle_data.tools as t
from pages.tle_data.OrbitPropagator import OrbitPropagator as OP
import streamlit as st
import numpy as np

# time parameters
tspan=3600*24*1.0
dt=10.0

#central body
cb=pd.earth

if __name__ == '__main__':
    #iss
    op0 = OP(t.tle2coes('pages/tle_data/iss.txt'), tspan, dt, coes=True)

    #hubble space telescope
    op1 = OP(t.tle2coes('pages/tle_data/hst.txt'), tspan, dt, coes=True)

    #ops-sat
    op2 = OP(t.tle2coes('pages/tle_data/ops-sat.txt'), tspan, dt, coes=True)   

    t.plot_n_orbits([op0.rs, op1.rs, op2.rs], labels=['ISS', 'Hubble', 'ops-sat'], streamlit=True)

    # Display ISS altitude at initial time
    altitude_iss = np.linalg.norm(op0.rs[0]) - cb['radius']
    st.write(f"ISS Altitude at Initial Time: {round(altitude_iss,2)} km")

    altitude_hst = np.linalg.norm(op1.rs[0]) - cb['radius']
    st.write(f"Hubble Space telescope Altitude at Initial Time: {round(altitude_hst,2)} km")

    altitude_ops = np.linalg.norm(op2.rs[0]) - cb['radius']
    st.write(f"OPS SAT Altitude at Initial Time: {round(altitude_ops,2)} km")

    
