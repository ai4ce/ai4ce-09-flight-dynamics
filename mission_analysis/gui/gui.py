"""
Demo class for the streamlit GUI.
"""
import streamlit as st
from mission_analysis.main import calculate_orbit_velocity


def app():
    st.title("Repo Template GUI")
    st.write("Welcome to the Default GUI!")

    st.write("This is a demo function from the main script. Enter an orbit altitude in km:")
    altitude = st.number_input("Altitude", value=450, step=1, format="%d")
    m_earth = 5.9721e24  # kg
    r_earth = 6371  # km
    st.write(
        "Your orbital velocity in m/s: ",
        calculate_orbit_velocity(planet_mass=m_earth, radius=(r_earth + altitude) * 1000),
    )


# This check allows the script to be run standalone (useful for development/testing)
if __name__ == "__main__":
    app()
